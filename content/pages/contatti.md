---
title: Contatti
date: 2021-07-06T17:21:41+02:00
showDate: false
---

## [ Cellulare: 393 9190 143](tel:+393939190143)

## [ Cellulare: 392 4344 611](tel:+393924344611)

---

## Ambulatorio a Siracusa, Via Senatore Di Giovanni 27

### [ Telefono: 0931 415167](tel:+390931415167)

### [ Indicazioni stradali con Google Maps](https://www.google.com/maps/dir//Via%20Senatore%20Di%20Giovanni%2027,%2096100%20Siracusa%20SR,%20Italy)

<iframe width="100%" height="600px" frameborder="0" src="https://umap.openstreetmap.fr/en/map/ambulatorio-di-siracusa-dr-carmelo-musco_634887?scaleControl=true&amp;miniMap=false&amp;scrollWheelZoom=true&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=false&amp;searchControl=false&amp;tilelayersControl=false&amp;embedControl=false&amp;datalayersControl=false&amp;onLoadPanel=none&amp;captionBar=true/37.08364/15.29111"></iframe>

---

## Studio a Priolo Gargallo (SR), Via Giulio Verne 11

### [ Telefono: 0931 767854](tel:+390931767854)

### [ Indicazioni stradali con Google Maps](https://www.google.com/maps/dir//Via%20Giulio%20Verne%2011,%2096010%20Priolo%20Gargallo%20SR,%20Italy)

<iframe width="100%" height="600px" frameborder="0" src="https://umap.openstreetmap.fr/en/map/studio-a-priolo-gargallo-dentista-dr-carmelo-musco_634888?scaleControl=true&amp;miniMap=false&amp;scrollWheelZoom=true&amp;zoomControl=true&amp;allowEdit=false&amp;moreControl=false&amp;searchControl=false&amp;tilelayersControl=false&amp;embedControl=false&amp;datalayersControl=false&amp;onLoadPanel=none&amp;captionBar=true/37.15916/15.18944"></iframe>

---

#### [ E-Mail: dentistacarmelomusco@gmail.com](mailto:dentistacarmelomusco@gmail.com)

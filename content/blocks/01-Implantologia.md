---
title: Implantologia orale
image: /blocks/img/implantologia.jpg
---

- Master Universitario di II livello in Implantologia e protesi dentarie: Nuove tecnologie
- [Pubblicazione scientifica](https://media.zwp-online.info/archiv/pub/4be9300d602ca/#13)
- [Impianti Bicon®](http://www.bicon.com/)

---
title: Quanto costa un'otturazione estetica di un molare?
date: 2020-07-26T13:33:13
tags:
  - ortodonzia
  - otturazione
  - estetica
  - molare
image: "/posts_pics/otturazione_estetica.jpg"
---

Un'Otturazione fatta bene prevede diversi passaggi:

 1. Rimozione del tessuto duro decalcificato, sia esso smalto o dentina
 2. Ultimato il procedimento precedente, se necessario, stendere sul fondo della cavità un isolante
 3. Procedere alla mordenzatura dei tessuti duri avendo cura di rispettare la dentina
 4. Stendere un primer/bonding (un collante in grado di legarsi sia al dente sia al materiale estetico utilizzato per il riempimento della cavità)
 5. Modellare il materiale di riempimento per ripristinare la morfologia occlusale, ossia la forma che originariamente aveva il dente in quel punto prima di essere distrutto dal processo carioso
 6. Controllare e funzionalizzare i contatti occlusali sia con i denti contigui sia con gli antagonisti
 7. Lucidare

Se un molare come quello sotto mostrato mi impegna per 1 ora di lavoro chiedo 400 Euro. 

Ad alcuni sembra eccessivo, anche perchè paragonato a "simili" otturazioni costate 100-150 Euro. Queste otturazioni io le ho viste e non posso dire se tutti i passaggi sopra elencati sono stati eseguiti. Tuttavia, ho potuto apprezzare il passaggio 5: manca il ripristino della morfologia occlusale e l'otturazione **è assolutamente piatta** e, conseguentemente, **priva di qualunque attività funzionale**. *Un'otturazione piatta di un molare costata 100-150 Euro* è molto costosa perché il paziente è privato della funzione di un molare e, molto spesso, non ne ha consapevolezza, purtroppo.

Nel caso sotto illustrato, un soggetto giovane,  si tratta di una carie in  che ha coinvolto il 1° molare permanente superiore che è il dente più importante tra tutti i denti e che è chiamato **la Chiave di Occlusione**  per la grande mole di funzioni che è chiamato a svolgere e in esso è stata coinvolta la cuspide più grande tra tutte le cuspidi chiamata CUSPIDE REGINA non solo per le dimensioni, ma soprattutto perchè è grazie ad essa che il molare riesce a svolgere le proprie funzioni (una cuspide è il rilievo smusso che sentiamo sui denti con la lingua).

![](/posts_pics/quanto_costa_un_otturazione_estetica_di_un_molare/1.jpg)

Io sono molto sodisfatto di questa otturazione perchè sono consapevole di avere ripristinato, insieme alla integrità del molare, importanti funzioni che erano andate perdute. La nota dolente del costo ritengo possa essere superata se il paziente capisce l'importanza dell'intervento. 

Per coloro, invece, che ad un costo più basso (ma più alto) hanno avuto ripristinati i loro molari con otturazioni piatte, dico loro che mi dispiace.

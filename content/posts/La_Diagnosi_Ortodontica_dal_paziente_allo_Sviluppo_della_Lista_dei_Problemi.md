---
title: "La Diagnosi Ortodontica: dal paziente allo Sviluppo della Lista dei Problemi"
date: 2018-08-24T09:00:39
tags: 
  - ortodonzia
image: "/posts_pics/diagnosi_ortodontica.jpg"
---

Concretizzare in modo completo una diagnosi ortodontica è fondamentale soprattutto per le ricadute sul piano di trattamento.

La sua realizzazione è affidata al cosiddetto "sviluppo della lista dei problemi": questa è l'unica strada per formulare una 

diagnosi ortodontica calzante con l'effettivo disturbo lamentato dal paziente.

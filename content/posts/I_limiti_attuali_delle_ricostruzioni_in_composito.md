---
title: I limiti attuali delle ricostruzioni in composito
date: 2019-01-05T14:46:45
tags: 
  - conservativa
  - morfologia occlusale
image: "/posts_pics/limiti_delle_ricostruzioni_in_composito.png"
---

Sono principalmente 3 i limiti delle ricostruzioni in composito:

1. la superficie di composito a contatto con l'aria (nella figura "A") polimerizza male per inibizione ossidativa esercitata dall'ossigeno
2. il limite tra la cavità dentaria e la massa di composito (nella figura "B") può essere il punto di insorgenza di carie secondaria
3. la morfologia occlusale originaria viene irrimediabilmente perduta e sostituita con una nuova morfologia occlusale sicuramente meno funzionale della originaria

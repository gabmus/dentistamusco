---
title: Cosa significa "Allineare i denti"
date: 2019-01-05T12:59:24
tags:
  - ortodonzia
  - occlusione
image: "/posts_pics/allineare_i_denti.png"
---

## Domanda 1

Quando si dice che un soggetto deve "allineare" i denti o che i suoi denti sono o non sono "ben allineati" a quale linea ci si riferisce?

## Domanda 2

E quando si dice che un soggetto ha "i denti storti" e li deve "raddrizzare" o che "non ha i denti diritti" a quale "raddrizzamento" ci si riferisce?

---

La prima domanda si riferisce alle *linee dentali* mentre la seconda si riferisce alle *anomalie di posizione dei denti*.

Tuttavia, considerando che anche le anomalie di posizione dei denti determinano alterazione nelle linee dentali, è possibile ricollegare entrambe le domande ad anomalie alle linee dentali. Quasi un secolo fa Angle parlava delle linee dentali affermando che la BO-line mandibolare doveva essere coincidente con la CF-line mascellare. Tali linee, tuttavia non sono visibili né nel paziente allorquando occlude né nei modelli di gesso quando sono tra loro articolati. 
Ultimamente si è aggiunta nella valutazione del caso ortodontico un'altra linea dentale: la *linea estetica delle arcate dentarie*. Tale linea idealmente la si ottiene collegando le superfici facciali dei denti posteriori (molari e premolari) e la superficie buccali dei denti anteriori ottenendosi  un arco dentale ben visibile quando il paziente sorride. Tale arco viene poi studiato in un contesto di coordinate spaziali ben precise (piano orizzontale, piano sagittale mediano e piano sagittale coronale) ed il tutto inserito all'interno di una banca dati che interessano altre caratteristiche morfologiche e dentali del soggetto. Da tale banca dati scaturisce la diagnosi di malocclusione dentale. 

Quindi, ciò che i denti evidenziano di *strano* cioè  inestetismi, denti mal posizionati in arcata ed assenza di un' estetica soddisfacente viene considerato come parte di un puzzle più grande. I denti *storti* o *non allineati* necessitano quindi di una gestione accurata che va al di là dei denti stessi.

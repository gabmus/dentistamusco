---
title: Nuova Tecnica Esclusiva per le Ricostruzioni in Composito
date: 2019-01-05T15:23:40
tags: 
  - ripristino morfologia occlusale
  - carie
  - composito
image: "/posts_pics/ricostruzioni_composito.png"
---

E noto che le varie tecniche di ricostruzione in composito presentano dei limiti, soprattutto per il ripristino della originaria morfologia occlusale -qualora si intervenga su un dente mai curato-.  Infatti, la rimozione della carie in un dente mai curato in precedenza comporterà  la perdita in modo irrimediabile della sua morfologia occlusale. Con tale tecnica si riesce a *ripristinare la originaria morfologia* tutto a vantaggio della funzione occlusale.
Vediamo due casi di soggetti di 16 anni.

## Soggetto 1

Prima della rimozione della carie:

![soggetto 1 prima della rimozione della carie](/posts_pics/ricostruzioni_composito.png)

Dopo la rimozione della carie:

![soggetto 1 dopo la rimozione della carie](/posts_pics/nuova_tecnica_esclusiva_per_le_ricostruzioni_in_composito/1.png)

Dopo la ricostruzione in composito con ripristino della morfologia occlusale originaria:

![soggetto 1 dopo la ricostruzione in composito con ripristino della morfologia occlusale originaria](/posts_pics/nuova_tecnica_esclusiva_per_le_ricostruzioni_in_composito/2.png)

## Soggetto 2

Prima della rimozione della carie:

![soggetto 2 prima della rimozione della carie](/posts_pics/nuova_tecnica_esclusiva_per_le_ricostruzioni_in_composito/3.png)

Dopo la rimozione della carie:

![soggetto 2 dopo la rimozione della carie](/posts_pics/nuova_tecnica_esclusiva_per_le_ricostruzioni_in_composito/4.png)

Dopo la ricostruzione in composito con ripristino della morfologia occlusale originaria:

![soggetto 2 dopo la ricostruzione in composito con ripristino della morfologia occlusale originaria](/posts_pics/nuova_tecnica_esclusiva_per_le_ricostruzioni_in_composito/5.png)


---
title: "La Diagnosi Ortodontica: rispondere a 250 quesiti secondo il POA (problem-oriented approach)"
date: 2018-08-10T11:32:39
tags:
  - ortodonzia
image: "/posts_pics/250_quesiti.jpg"
---

La diagnosi ortodontica dovrebbe precedere sempre la terapia sia che si tratti di apparecchio mobile sia che si tratti di ortodonzia fissa.

Rispetto al passato in cui il procedimento diagnostico era affidato alla memoria del dentista (approccio memoria orientato), l'attuale approccio adottato a livello internazionale è il POA, acronimo che indica problem-oriented approach. Con tale approccio il dentista si astiene dal formulare possibili diagnosi fin dal primo contatto con il paziente. Il suo compito fondamentale deve essere raccogliere quanti più dati possibili inerenti il motivo per cui il paziente ha sentito il bisogno di chiedere un incontro con il dentista ortodontista.

Il massimo delle informazioni che il dentista potrà ottenere rappresentano delle risposte a circa 250 quesiti. Ma perché proprio 250? Questo numero risulta dalla più importante ed accreditata classificazione delle patologie ortodontiche che è quella di Ackerman e Profitt. Se domani l'Autorità Scientifica Ortodontica decidesse di adottare una classificazione differente e più particolareggiata i quesiti potrebbero raddoppiare.
